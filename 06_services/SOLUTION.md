# Lösung zur Übung Services


#### 1.
```
cd exercise/src/app
ng generate service image
```

#### 2.
[image.service.ts](solution/src/app/image.service.ts)

#### 3.
[slideshow.component.ts](solution/src/app/slideshow/slideshow.component.ts)
