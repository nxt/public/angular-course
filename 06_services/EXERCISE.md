# Übung Services

#### 0. Führe einen `npm install` und `npm run start` im `exercise` Ordner aus.

#### 1. Erstelle im `app` Verzeichnis einen neuen Service mit dem Namen `image`.

#### 2. Erweitere den `image` Service mit folgender Funktionalität:
* Definiere ein Array mit verschiedenen URLs.
* Erstelle eine Methode, mit der man eine URL holen kann.
* Erstelle eine Methode, mit der man die URL ändern kann.

#### 3. Binde den neuen `image` Service in die `slideshow` Komponente ein und ersetze die dort verwendeten URLs.

#### Zusatzübung
1. Füge in der `slideshow` Komponente die `image` Komponente drei mal hinzu. Einmal soll das vorherige, einmal das aktuelle und einmal das nächste Bild angezeigt werden.
2. Verschiebe den `image` Service in die `image` Komponente, so dass die Kommunikation über den Service läuft und nicht mehr über die Inputs und Outputs.
