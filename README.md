# Angular Kurs

## Installationen
* [Node.js](https://nodejs.org/en/)
* [git](https://git-scm.com)
* WebStorm oder Visual Studio Code

## Code auschecken
`git clone git@gitlab.com:nxt/public/angular-course.git`

## Übungen
1. [Bootstrapping](01_bootstrapping/EXERCISE.md)
2. [Komponenten Teil 1](02_components_1/EXERCISE.md)
3. [Komponenten Teil 2](03_components_2/EXERCISE.md)
4. [Direktiven](04_directives/EXERCISE.md)
5. [Pipes](05_pipes/EXERCISE.md)
6. [Services](06_services/EXERCISE.md)
7. [Router](07_router/EXERCISE.md)
8. [Forms](08_forms/EXERCISE.md)
9. [HttpClient](09_http/EXERCISE.md)
10. [Testing](10_testing/EXERCISE.md)
11. [Change Detection](11_change_detection/EXERCISE.md)

## Lösungen
1. [Bootstrapping](01_bootstrapping/SOLUTION.md)
2. [Komponenten Teil 1](02_components_1/SOLUTION.md)
3. [Komponenten Teil 2](03_components_2/SOLUTION.md)
4. [Direktiven](04_directives/SOLUTION.md)
5. [Pipes](05_pipes/SOLUTION.md)
6. [Services](06_services/SOLUTION.md)
7. [Router](07_router/SOLUTION.md)
8. [Forms](08_forms/SOLUTION.md)
9. [HttpClient](09_http/SOLUTION.md)
10. [Testing](10_testing/SOLUTION.md)
11. [Change Detection](11_change_detection/SOLUTION.md)
