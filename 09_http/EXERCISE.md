# Übung HTTP

#### 0. Führe einen `npm install` und `npm run start` im `exercise` Ordner aus.

#### 0. Einleitung

Moderne Browser verhindern den Zugriff auf externe Ressourcen, die nicht die gleiche Herkunft haben. Dieses Sicherheitskonzept ist unter dem Namen Same-Origin-Policy (SOP) bekannt.

Aus diesem Grund müssen wir in Angular einen Proxy definieren, um auf die JSON Datei, die in GitLab abgelegt ist, zuzugreifen.

#### 1. Erstelle eine Datei mit dem Namen `proxy.conf.json` im Root-Verzeichnis `exercise` mit folgendem Inhalt:
```
{
  "/nxt": {
    "target": "https://gitlab.com",
    "secure": false,
    "changeOrigin": true
  }
}
```
Tipp: [https://angular.io/guide/build#proxying-to-a-backend-server]()

#### 2. Füge in der `angular.json` Datei den Namen der soeben erstellen Proxy Konfiguration ein.
Tipp: [https://angular.io/guide/build#proxying-to-a-backend-server]()

#### 3. Ändere den `image` Service so, dass die URLs neu via HTTP gelesen werden
URL: [https://gitlab.com/nxt/public/angular-course/raw/master/unsplashed-urls.json]()

Tipp: Durch die Proxy Konfiguration, können die Daten via '/nxt/public/angular-course/raw/master/unsplashed-urls.json' geladen werden.

#### Zusatzübung
Verwende den Server im Ordner `server`. Mit `npm start` lässt er sich starten.
Der Proxy muss entsprechend angepasst werden. Danach kann man URLs lesen, bearbeiten, hinzufügen und löschen.
