import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FullscreenComponent} from './fullscreen/fullscreen.component';
import {SlideshowComponent} from './slideshow/slideshow.component';

const routes: Routes = [
  {path: 'fullscreen', component: FullscreenComponent},
  {path: 'slideshow', component: SlideshowComponent},
  {
    path: '',
    redirectTo: 'slideshow',
    pathMatch: 'full'
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
