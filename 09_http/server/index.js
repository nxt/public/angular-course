const express = require("express");
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());

const urls = {};
let index = 0;
urls[index++] = 'https://images.unsplash.com/photo-1554513748-aa63f6553978?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60';
urls[index++] = 'https://images.unsplash.com/photo-1554412055-17d57b3b9e40?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1932&q=80';

app.listen(3000, () => {
    console.log("Server running on port 3000");
    console.log("GET\thttp://localhost:3000/urls");
    console.log("POST\thttp://localhost:3000/urls");
    console.log("GET\thttp://localhost:3000/urls/:id");
    console.log("DELETE\thttp://localhost:3000/urls/:id");
    console.log("PUT\thttp://localhost:3000/urls/:id");
});

app.get("/api/urls", (req, res) => {
    const result = [];
    Object.keys(urls).forEach(function (id) {
        result.push({id, url: urls[id]})
    });
    res.json(result);
});

app.get("/api/urls/:id", (req, res) => {
    const id = req.params.id;
    if (!urls[id]) {
        res.sendStatus(404);
    } else {
        res.json({id: id, url: urls[id]});
    }
});

app.post("/api/urls", (req, res) => {
    const id = index++;
    urls[id] = req.body.url;
    res.json({id: id, url: req.body.url});
});

app.put("/api/urls/:id", (req, res) => {
    const id = req.params.id;
    urls[id] = req.body.url;
    res.json({id: id, url: req.body.url});
});

app.delete("/api/urls/:id", (req, res) => {
    const id = req.params.id;
    delete urls[id];
    res.sendStatus(200);
});
