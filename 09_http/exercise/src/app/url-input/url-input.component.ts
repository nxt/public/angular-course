import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-url-input',
  templateUrl: './url-input.component.html',
  styleUrls: ['./url-input.component.css']
})
export class UrlInputComponent implements OnInit {
  @Output() urlChange = new EventEmitter<string>();

  url?: string;

  constructor() {
  }

  ngOnInit(): void {
  }

  onOk(): void {
    this.urlChange.emit(this.url);
  }
}
