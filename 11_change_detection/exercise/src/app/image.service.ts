import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private index = 0;

  constructor(private httpClient: HttpClient) {
  }

  getUrl(): Observable<string> {
    return this.getUrls()
      .pipe(map(urls => urls[this.index % urls.length]));
  }

  changeUrl(): void {
    this.index++;
  }

  private getUrls(): Observable<string[]> {
    return this.httpClient.get<string[]>('/nxt/public/angular-course/raw/master/unsplashed-urls.json');
  }

}
