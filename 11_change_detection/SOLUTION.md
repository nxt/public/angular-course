# Lösung zur Übung Change Detection


#### 1.

[app.component.ts](solution/src/app/app.component.ts)

[fullscreen.component.ts](solution/src/app/fullscreen/fullscreen.component.ts)

[header.component.ts](solution/src/app/header/header.component.ts)

[image.component.ts](solution/src/app/image/image.component.ts)

[slideshow.component.ts](solution/src/app/slideshow/slideshow.component.ts)

[url-input.component.ts](solution/src/app/url-input/url-input.component.ts)
