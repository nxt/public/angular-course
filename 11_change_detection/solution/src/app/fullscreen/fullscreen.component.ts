import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-fullscreen',
  templateUrl: './fullscreen.component.html',
  styleUrls: ['./fullscreen.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FullscreenComponent implements OnInit {
  url?: string;

  constructor(activatedRoute: ActivatedRoute) {
    activatedRoute.queryParams.subscribe(params => this.url = params.url);
  }

  ngOnInit(): void {
  }

}
