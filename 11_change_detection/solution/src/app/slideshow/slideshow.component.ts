import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ImageService} from '../image.service';

@Component({
  selector: 'app-slideshow',
  templateUrl: './slideshow.component.html',
  styleUrls: ['./slideshow.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlideshowComponent implements OnInit {

  url?: string;

  constructor(private imageService: ImageService,
              private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.loadUrl();
  }

  onImageClick(): void {
    this.imageService.changeUrl();
    this.loadUrl();
  }

  private loadUrl(): void {
    this.imageService.getUrl().subscribe(url => {
      this.url = url;
      this.changeDetectorRef.markForCheck();
    });
  }
}
