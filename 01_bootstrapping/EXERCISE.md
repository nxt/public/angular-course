# Übung Bootstrapping

#### 1. Generiere eine neue Angular Applikation im Verzeichnis `exercise` mit den folgenden Eigenschaften:
* Name: slideshow
* Angular Routing: Ja
* Styleheet Format: CSS
* Verwende den strikten Modus

Tipp: https://angular.io/cli/new

Achte darauf, dass die neue Applikation direkt im Verzeichnis `exercise` liegt. Es soll kein zusätzliches Verzeichnis `slideshow` erstellt werden.
    
#### 2. Starte die Angular Applikation

#### 3. Lösche den gesamten Inhalt aus der [app.component.html](exercise/src/app/app.component.html) Datei.

#### 4. Kommentiere den Inhalt in der [app.component.spec.ts](exercise/src/app/app.component.html) Datei aus.
Für alle in Zukunft erstellten Komponenten, Direktiven, Pipes und Services soll der Test immer auskommentiert werden.

#### 5. Erstelle einen produktiven Build der Applikation. 
Tipp: [https://angular.io/cli/build]()
