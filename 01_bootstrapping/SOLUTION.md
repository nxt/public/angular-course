# Lösung zur Übung Bootstrapping

#### 1. 
```
npm install -g @angular/cli
cd 01_bootstrapping/exercise/
ng new --directory=. slideshow --strict
? Do you want to enforce stricter type checking and stricter bundle budgets in the workspace? -> y
? Would you like to add Angular routing? (y/N) -> y
? Which stylesheet format would you like to use? (Use arrow keys) -> CSS
```

#### 2. 
```
ng serve
```
oder
```
npm run start
```

#### 3.
[app.component.html](solution/src/app/app.component.html)

#### 4.
[app.component.spec.ts](solution/src/app/app.component.spec.ts)

#### 5.
```
ng build --prod
```
