import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'customDate'
})
export class CustomDatePipe implements PipeTransform {

  // tslint:disable-next-line:no-any
  transform(value: Date, args?: any[]): string {
    return value.toLocaleDateString('de-CH', {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'});
  }

}
