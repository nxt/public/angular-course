# Übung Router

#### 0. Führe einen `npm install` und `npm run start` im `exercise` Ordner aus.

#### 1. Erstelle im `app` Verzeichnis eine neue Komponente mit dem Namen `fullscreen`.

#### 2. Erweitere das Modul `AppRoutingModule` mit den folgenden Routing Definitionen:
* `/fullscreen` soll die `fullscreen` Komponente anzeigen.
* `/slideshow` soll die `slideshow` Komponente anzeigen.
* `/` soll ebenfalls die `slideshow` Komponente anzeigen.

#### 3. Entferne die `slideshow` Komponente aus der `app` Komponente und füge stattdessen ein Router Outlet hinzu.

#### 4. Füge der `slideshow` Komponente einen Button hinzu, welcher zur `fullscreen` Komponente navigiert. Als Query Parameter soll die URL des aktuellen Bildes mitgegeben werden. 
Tipp: [https://angular.io/api/router/RouterLink#routerlink]()

#### 5. Füge einen globalen Style für den Button hinzu.

#### 6. Füge die `image` Komponente der `fullscreen` Komponente hinzu.
Tipp: Die benötigte URL kannst du aus dem Query Parameter lesen.

#### 7. Füge der `fullscreen` Komponente einen Button hinzu, um auf die Slideshow zu springen.

#### Zusatzübung
Ersetze den Query Parameter durch einen normalen Parameter. Gib anstelle der URL nur noch den Index via Parameter (/fullscreen/1) weiter.
