# Lösung zur Übung Router


#### 1.
```
cd exercise/src/app
ng generate component fullscreen
```

#### 2.
[app-routing.module.ts](solution/src/app/app-routing.module.ts)

#### 3.
[app.component.html](solution/src/app/app.component.html)

#### 4.
[slideshow.component.html](solution/src/app/slideshow/slideshow.component.html)

#### 5.
[styles.css](solution/src/styles.css)

#### 6.
[fullscreen.component.html](solution/src/app/fullscreen/fullscreen.component.html)
[fullscreen.component.ts](solution/src/app/fullscreen/fullscreen.component.ts)

#### 7.
[fullscreen.component.html](solution/src/app/fullscreen/fullscreen.component.html)
