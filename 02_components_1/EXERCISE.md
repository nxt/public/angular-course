# Übung Komponenten Teil 1

#### 0. Führe einen `npm install` und `npm run start` im `exercise` Ordner aus.

#### 1. Erstelle eine neue Komponente namens `slideshow` welche im Verzeichnis `app` liegt.

#### 2. Erstelle eine neue Komponente namens `header` welche im Verzeichnis `app` liegt.

#### 3. Füge die Komponente `slideshow` in der `app` Komponente ein.

#### 4. Füge die Komponente `header` in der `slideshow` Komponente ein.

#### 5. Füge den Titel "Slideshow" als Variable in die Komponente `header` ein und gebe sie im Template aus.

#### 6. Definiere eine applikationsübergreifende Schriftart, welche dir gefällt.

#### 7. Zentriere den Titel in der `header` Komponente.
Tipp: [https://angular.io/guide/component-styles#host]()

#### 8. Erstelle eine neue Komponente namens `image` welche im Verzeichnis `app` liegt.

#### 9. Füge die Komponente `image` in der `slideshow` Komponente ein.

#### 10. Füge ein Bild in die `image` Komponente ein. Die URL des Bildes sollte aus einer Variable kommen.
Tipp: [https://unsplash.com]()

#### 11. Zentriere das Bild in der `image` Komponente und füge einen `box-shadow` hinzu.
Tipp: [https://www.cssmatic.com/box-shadow]()


#### Zusatzübung
Versuch die folgenden strukturellen Direktiven anzuwenden:
* [NgForOf](https://angular.io/api/common/NgForOf)
* [NgIf](https://angular.io/api/common/NgIf)
* [NgSwitch](https://angular.io/api/common/NgSwitch)

Versuch die folgenden Direktiven anzuwenden:
* [NgClass](https://angular.io/api/common/NgClass)
* [NgStyle](https://angular.io/api/common/NgStyle)
