# Lösung zur Übung Komponenten Teil 1

#### 1. 
```
cd exercise/src/app
ng generate component slideshow
```

#### 2. 
```
cd exercise/src/app
ng generate component header
```

#### 3.
[app.component.html](solution/src/app/app.component.html)


#### 4.
[slideshow.component.html](solution/src/app/slideshow/slideshow.component.html)


#### 5.
[header.component.ts](solution/src/app/header/header.component.ts)

[header.component.html](solution/src/app/header/header.component.html)


#### 6.
[styles.css](solution/src/styles.css)


#### 7.
[header.component.css](solution/src/app/header/header.component.css)


#### 8.
```
cd exercise/src/app
ng generate component image
```


#### 9.
[slideshow.component.html](solution/src/app/slideshow/slideshow.component.html)

#### 10.
[image.component.ts](solution/src/app/image/image.component.ts)

[image.component.html](solution/src/app/image/image.component.html)

#### 11.
[header.component.css](solution/src/app/image/image.component.css)
