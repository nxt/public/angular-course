import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {

  @Input() url: string | undefined;
  @Output() imageClick = new EventEmitter<void>();

  constructor() {
  }

  ngOnInit(): void {
  }

  onImageClick(): void {
    this.imageClick.emit();
  }
}
