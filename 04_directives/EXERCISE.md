# Übung Direktiven

#### 0. Führe einen `npm install` und `npm run start` im `exercise` Ordner aus.

#### 1. Erstelle im `app` Verzeichnis eine neue Direktive mit dem Namen `tooltip`.

#### 2. Erweitere die `tooltip` Direktive mit der Funktionalität, dass sie einen Input Wert als `title` Attribut dem HTML Element hinzufügt.

#### 3. Füge die `tooltip` Direktive der `header` Komponente hinzu. So dass der Titel 'Slideshow' einen Tooltip hat.
