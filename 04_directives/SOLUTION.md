# Lösung zur Übung Direktiven


#### 1.
```
cd exercise/src/app
ng generate directive tooltip
```

#### 2.
[tooltip.directive.ts](solution/src/app/tooltip.directive.ts)

#### 3.
[header.component.html](solution/src/app/header/header.component.html)
