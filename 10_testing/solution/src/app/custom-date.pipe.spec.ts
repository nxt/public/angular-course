import {CustomDatePipe} from './custom-date.pipe';

describe('CustomDatePipe', () => {
  it('validate transformation', () => {
    const pipe = new CustomDatePipe();
    expect(pipe.transform(new Date(2019, 3, 1))).toEqual('Montag, 1. April 2019');
  });
});
