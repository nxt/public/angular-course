import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {UrlInputComponent} from './url-input.component';
import {FormsModule} from '@angular/forms';

describe('UrlInputComponent', () => {
  let component: UrlInputComponent;
  let fixture: ComponentFixture<UrlInputComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [UrlInputComponent],
      imports: [FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlInputComponent);
    component = fixture.componentInstance;
  });

  it('should emit url when pressed on ok', () => {
    let emittedUrl: string | null = null;
    component.urlChange.subscribe((url: string) => (emittedUrl = url));
    component.url = 'test';
    component.onOk();
    fixture.detectChanges();
    expect(emittedUrl + '').toEqual('test');
  });
});
