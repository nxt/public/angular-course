import { ComponentFixture, fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import {SlideshowComponent} from './slideshow.component';
import {HeaderComponent} from '../header/header.component';
import {ImageComponent} from '../image/image.component';
import {UrlInputComponent} from '../url-input/url-input.component';
import {CustomDatePipe} from '../custom-date.pipe';
import {FormsModule} from '@angular/forms';
import {TooltipDirective} from '../tooltip.directive';
import {ImageService} from '../image.service';
import {Subject} from 'rxjs';
import createSpy = jasmine.createSpy;
import {RouterTestingModule} from '@angular/router/testing';


describe('SlideshowComponent', () => {
  let component: SlideshowComponent;
  let fixture: ComponentFixture<SlideshowComponent>;

  const imageService = {
    changeUrl: createSpy('changeUrl'),
    getUrl: createSpy('getUrl').and.callFake(() => new Subject())
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SlideshowComponent, HeaderComponent, ImageComponent, UrlInputComponent, CustomDatePipe, TooltipDirective],
      imports: [FormsModule, RouterTestingModule],
      providers: [{provide: ImageService, useValue: imageService}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideshowComponent);
    component = fixture.componentInstance;
  });

  it('should call getUrl and changeUrl in ImageService', fakeAsync(() => {
    component.onImageClick();
    fixture.detectChanges();
    expect(imageService.getUrl).toHaveBeenCalled();
    expect(imageService.changeUrl).toHaveBeenCalled();
  }));

});
