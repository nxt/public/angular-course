import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {HeaderComponent} from './header.component';
import {CustomDatePipe} from '../custom-date.pipe';
import {TooltipDirective} from '../tooltip.directive';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent, CustomDatePipe, TooltipDirective]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
  });

  it('today should be set', () => {
    fixture.detectChanges();
    expect(component.today.toDateString()).toEqual(new Date().toDateString());
    expect(fixture.elementRef.nativeElement.innerText).toEqual(`Slideshow - ${new CustomDatePipe().transform(new Date())}`);
  });
});
