import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {ImageComponent} from './image.component';
import {By} from '@angular/platform-browser';

describe('ImageComponent', () => {
  let component: ImageComponent;
  let fixture: ComponentFixture<ImageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ImageComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageComponent);
    component = fixture.componentInstance;
  });

  it('should not render the img tag if the url is null', () => {
    component.url = undefined;
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('img'))).toBeNull();
  });

  it('should render the img tag if the url is set', () => {
    component.url = 'url';
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('img'))).toBeTruthy();
  });

});
