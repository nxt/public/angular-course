import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {FullscreenComponent} from './fullscreen.component';
import {ImageComponent} from '../image/image.component';
import {RouterTestingModule} from '@angular/router/testing';
import {Subject} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {TooltipDirective} from '../tooltip.directive';

describe('FullscreenComponent', () => {
  let component: FullscreenComponent;
  let fixture: ComponentFixture<FullscreenComponent>;
  const mockRoute = {queryParams: new Subject<{ url: string }>()};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FullscreenComponent, ImageComponent, TooltipDirective],
      imports: [RouterTestingModule.withRoutes([])],
      providers: [{provide: ActivatedRoute, useValue: mockRoute}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullscreenComponent);
    component = fixture.componentInstance;
  });

  it('should fetch the url from the queryParam', () => {
    mockRoute.queryParams.next({url: 'test'});
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.url).toEqual('test');
  });

});

