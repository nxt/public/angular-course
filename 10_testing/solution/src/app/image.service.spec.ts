import {getTestBed, TestBed} from '@angular/core/testing';
import {ImageService} from './image.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

function executeTest(expected = 'test1'): void {
  const service: ImageService = TestBed.inject(ImageService);
  const httpMock = getTestBed().inject(HttpTestingController);
  service.getUrl().subscribe(data => {
    expect(data).toEqual(expected);
  });

  const req = httpMock.expectOne('/nxt/public/angular-course/raw/master/unsplashed-urls.json');
  expect(req.request.method).toBe('GET');
  req.flush(['test1', 'test2']);
}

describe('ImageService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('index should start at 0', () => {
    executeTest('test1');
  });

  it('index should be 1 after one change', () => {
    const service: ImageService = TestBed.inject(ImageService);
    service.changeUrl();
    executeTest('test2');
  });

  it('index should be 0 after two changes', () => {
    const service: ImageService = TestBed.inject(ImageService);
    service.changeUrl();
    service.changeUrl();
    executeTest('test1');
  });
});

