import {TooltipDirective} from './tooltip.directive';
import {Component} from '@angular/core';
import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {By} from '@angular/platform-browser';

@Component({
  template: `<h1 [appTooltip]="'i am a test tooltip'"></h1>`
})
class TestTooltipComponent {
}

describe('TooltipDirective', () => {

  let component: TestTooltipComponent;
  let fixture: ComponentFixture<TestTooltipComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TestTooltipComponent, TooltipDirective]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestTooltipComponent);
    component = fixture.componentInstance;
  });

  it('title should be applied to h1', () => {
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('h1'));

    expect(element.nativeElement.title).toEqual('i am a test tooltip');
  });
});
