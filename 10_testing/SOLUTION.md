# Lösung zur Übung Testing


#### 1.
```
npm test
```

#### 2.
[custom-date.pipe.spec.ts](solution/src/app/custom-date.pipe.spec.ts)

#### 3.
[image.service.spec.ts](solution/src/app/image.service.spec.ts)

#### 4. 
[url-input.component.spec.ts](solution/src/app/url-input/url-input.component.spec.ts)

#### 5. 
[image.component.spec.ts](solution/src/app/image/image.component.spec.ts)

#### 6. 
[header.component.spec.ts](solution/src/app/header/header.component.spec.ts)

#### 7. 
[fullscreen.component.spec.ts](solution/src/app/fullscreen/fullscreen.component.spec.ts)

#### 8. 
[slideshow.component.spec.ts](solution/src/app/slideshow/slideshow.component.spec.ts)

#### 9. 
[tooltip.directive.spec.ts](solution/src/app/tooltip.directive.spec.ts)
