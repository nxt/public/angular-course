# Übung Testing

#### 0. Führe einen `npm install` im `exercise` Ordner aus.

#### 1. Starte die Karma Tests

#### 2. Schreibe Tests für die `custom-date` Pipe.

#### 3. Schreibe Tests für den `image` Service.
Tipp: [https://angular.io/guide/testing#httpclienttestingmodule]()

#### 4. Schreibe Tests für die `url-input` Komponente.

#### 5. Schreibe Tests für die `image` Komponente.

#### 6. Schreibe Tests für die `header` Komponente.

#### 7. Schreibe Tests für die `fullscreen` Komponente.

#### 8. Schreibe Tests für die `slideshow` Komponente.

#### 9. Schreibe Tests für die `tooltip` Direktive.
