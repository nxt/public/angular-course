# Übung Forms

#### 0. Führe einen `npm install` und `npm run start` im `exercise` Ordner aus.

#### 1. Erstelle im `app` Verzeichnis eine neue Komponente mit dem Namen `url-input`.

#### 2. Füge die `url-input` Komponente der `slideshow` Komponente hinzu.

#### 3. Füge der `url-input` Komponente ein Input Text Feld hinzu welches validiert ob die Eingabe eine gültige URL ist.
Es kann entweder das Reactive Forms Module oder das Forms Module verwendet werden.

#### 4. Füge einen Knopf der `url-input` Komponente hinzu welcher die eingegebene URL via @Output an seine Parent Komponente weiterleitet. Den Knopf darf nur aktiviert sein, wenn die URL gültig ist.

#### 5. Erweitere die `slideshow` Komponente so, dass sie die URL der `url-input` Komponente entgegen nimmt und entsprechend anzeigt.

# Zusatzübung
Style das Input Form mit den folgenden Klassen:
* ng-valid
* ng-invalid
* ng-pending
* ng-pristine
* ng-dirty
* ng-untouched
* ng-touched
