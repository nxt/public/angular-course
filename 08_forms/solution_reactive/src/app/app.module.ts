import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SlideshowComponent} from './slideshow/slideshow.component';
import {HeaderComponent} from './header/header.component';
import {ImageComponent} from './image/image.component';
import {TooltipDirective} from './tooltip.directive';
import {CustomDatePipe} from './custom-date.pipe';
import {FullscreenComponent} from './fullscreen/fullscreen.component';
import {UrlInputComponent} from './url-input/url-input.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    SlideshowComponent,
    HeaderComponent,
    ImageComponent,
    TooltipDirective,
    CustomDatePipe,
    FullscreenComponent,
    UrlInputComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
