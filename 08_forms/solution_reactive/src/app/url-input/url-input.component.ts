import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-url-input',
  templateUrl: './url-input.component.html',
  styleUrls: ['./url-input.component.css']
})
export class UrlInputComponent implements OnInit {
  @Output() urlChange = new EventEmitter<string>();
  urlControl = new FormControl('', [Validators.required, Validators.pattern('(https?)://[^\\s/$.?#].[^\\s]*')]);

  constructor() {
  }

  ngOnInit(): void {
  }

  onOk(): void {
    this.urlChange.emit(this.urlControl.value);
  }
}
