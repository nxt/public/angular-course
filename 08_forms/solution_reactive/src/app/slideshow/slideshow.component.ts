import {Component, OnInit} from '@angular/core';
import {ImageService} from '../image.service';

@Component({
  selector: 'app-slideshow',
  templateUrl: './slideshow.component.html',
  styleUrls: ['./slideshow.component.css']
})
export class SlideshowComponent implements OnInit {

  url: string | undefined;

  constructor(private imageService: ImageService) {
  }

  ngOnInit(): void {
    this.url = this.imageService.getUrl();
  }

  onImageClick(): void {
    this.imageService.changeUrl();
    this.url = this.imageService.getUrl();
  }
}
