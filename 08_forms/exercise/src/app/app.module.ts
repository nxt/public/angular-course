import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SlideshowComponent} from './slideshow/slideshow.component';
import {HeaderComponent} from './header/header.component';
import {ImageComponent} from './image/image.component';
import {TooltipDirective} from './tooltip.directive';
import {CustomDatePipe} from './custom-date.pipe';
import {FullscreenComponent} from './fullscreen/fullscreen.component';

@NgModule({
  declarations: [
    AppComponent,
    SlideshowComponent,
    HeaderComponent,
    ImageComponent,
    TooltipDirective,
    CustomDatePipe,
    FullscreenComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
