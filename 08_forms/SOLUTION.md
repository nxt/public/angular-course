# Lösung zur Übung Forms


#### 1.
```
cd exercise/src/app
ng generate component url-input
```

#### 2.
- [slideshow.component.html (template-driven)](solution_template/src/app/slideshow/slideshow.component.html)
- [slideshow.component.html (reactive)](solution_reactive/src/app/slideshow/slideshow.component.html)

#### 3.
- [url-input.component.html (template-driven)](solution_template/src/app/url-input/url-input.component.html)
- [url-input.component.html (reactive)](solution_reactive/src/app/url-input/url-input.component.html)
- [url-input.component.ts (template-driven)](solution_template/src/app/url-input/url-input.component.ts)
- [url-input.component.ts (reactive)](solution_reactive/src/app/url-input/url-input.component.ts)

#### 4.
- [app.component.html (template-driven)](solution_template/src/app/app.component.html)
- [app.component.html (reactive)](solution_reactive/src/app/app.component.html)

#### 5.
- [slideshow.component.html (template-driven)](solution_template/src/app/slideshow/slideshow.component.html)
- [slideshow.component.html (reactive)](solution_reactive/src/app/slideshow/slideshow.component.html)
