import {Directive, ElementRef, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[appTooltip]'
})
export class TooltipDirective implements OnInit {

  @Input() appTooltip: string | undefined;

  constructor(private el: ElementRef) {
  }

  ngOnInit(): void {

    this.el.nativeElement.title = this.appTooltip;
  }

}
