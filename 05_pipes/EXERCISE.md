# Übung Pipes

#### 0. Führe einen `npm install` und `npm run start` im `exercise` Ordner aus.

#### 1. Erstelle im `app` Verzeichnis eine neue Pipe mit dem Namen `custom-date`.

#### 2. Erweitere die `custom-date` Pipe mit der Funktionalität, dass sie ein Datum entgegen nimmt und "schön" formatiert.

#### 3. Füge die `custom-date` Pipe der `header` Komponente hinzu. So dass zu dem Namen 'Slideshow' das heutige Datum angezeigt wird.
