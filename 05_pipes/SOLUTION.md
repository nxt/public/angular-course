# Lösung zur Übung Pipes


#### 1.
```
cd exercise/src/app
ng generate pipe custom-date
```

#### 2.
[custom-date.pipe.ts](solution/src/app/custom-date.pipe.ts)

#### 3.
[header.component.html](solution/src/app/header/header.component.html)

[header.component.ts](solution/src/app/header/header.component.ts)
