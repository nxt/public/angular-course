#!/bin/zsh

killall node

cd ../01_bootstrapping/solution       && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4201 &
cd ../../02_components_1/exercise     && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4202 &
cd ../../02_components_1/solution     && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4203 &
cd ../../03_components_2/exercise     && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4204 &
cd ../../03_components_2/solution     && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4205 &
cd ../../04_directives/exercise       && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4206 &
cd ../../04_directives/solution       && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4207 &
cd ../../05_pipes/exercise            && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4208 &
cd ../../05_pipes/solution            && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4209 &
cd ../../06_services/exercise         && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4210 &
cd ../../06_services/solution         && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4211 &
cd ../../07_router/exercise           && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4212 &
cd ../../07_router/solution           && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4213 &
cd ../../08_forms/exercise            && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4214 &
cd ../../08_forms/solution_template   && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4215 &
cd ../../08_forms/solution_reactive   && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4216 &
cd ../../09_http/exercise             && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4217 &
cd ../../09_http/solution             && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4218 &
cd ../../10_testing/exercise          && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4219 &
cd ../../10_testing/solution          && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4220 &
cd ../../11_change_detection/exercise && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4221 &
cd ../../11_change_detection/solution && npm i && ng update @angular/core @angular/cli && npm-check --update-all && npm audit fix && npm run build && ng serve --port=4222 &

read -s -k '?Press any key to stop all node processes.'
killall node
