# Übung Komponenten Teil 2

#### 0. Führe einen `npm install` und `npm run start` im `exercise` Ordner aus.

#### 1. Definiere die URL in der `image` Komponente als Input. Die URL soll als Variable in der `slideshow` Komponente definiert werden.

#### 2. Wenn auf das Bild in der `image` Komponente geklickt wird, soll die `slideshow` Komponente benachrichtigt werden und die URL des Bildes ändern.
